import 'package:flutter/material.dart';
import 'package:mcutil/uuid.dart';

class McDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final drawerItems = [
      DrawerHeader(
        child: Text('McUtils'),
      ),
      ListTile(
        title: Text('Uuid'),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return UuidPage(title: 'UUID');
              },
            ),
          );
        },
      )
    ];
    return Drawer(
      child: ListView.builder(
        itemCount: drawerItems.length,
        itemBuilder: (BuildContext context, int index) {
          return drawerItems[index];
        },
      ),
    );
  }
}
