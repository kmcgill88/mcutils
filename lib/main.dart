import 'package:corsac_jwt/corsac_jwt.dart';
import 'package:flutter/material.dart';
import 'package:mcutil/drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'McUtils',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'McUtils'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String jwt = '0.0.0';
  String header = 'Header';
  Map<String, dynamic> body = {};

  @override
  Widget build(BuildContext context) {
    final keys = body.map(
      (k, v) => MapEntry(
        k,
        Text(
          '$k: $v',
        ),
      ),
    );
    return Scaffold(
      drawer: McDrawer(),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: 200,
            height: 200,
            child: TextField(
              decoration: InputDecoration(hintText: 'Put Jwt here'),
              onChanged: (String text) {
                setState(
                  () {
                    try {
                      jwt = text;
                      final decoded = JWT.parse(text);
                      body = decoded.claims;
                      header = decoded.encodedHeader;
                    } on Exception {
                      jwt = '0.0.0';
                      body = {};
                      header = 'fail';
                    }
                  },
                );
              },
            ),
          ),
          for (var entry in body.entries)
            SelectableText(
              '${entry.key}: ${entry.value}',
              textAlign: TextAlign.right,
            ),
        ],
      ),
    );
  }
}
