import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class UuidPage extends StatefulWidget {
  UuidPage({Key key, @required this.title}) : super(key: key);


  final String title;

  @override
  _UuidPageState createState() => _UuidPageState();
}

class _UuidPageState extends State<UuidPage> {
  String _counter = Uuid().v4();

  void _incrementCounter() {
    setState(() {
      _counter = Uuid().v4();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SelectableText(
              _counter,
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Next UUID',
        child: Icon(Icons.refresh),
      ),
    );
  }
}
